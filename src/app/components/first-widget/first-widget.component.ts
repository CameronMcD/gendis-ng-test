import { Component, OnInit } from '@angular/core';
import { Config } from 'protractor';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-first-widget',
  templateUrl: './first-widget.component.html',
  styleUrls: ['./first-widget.component.css']
})
export class FirstWidgetComponent implements OnInit {

  configUrl: string = 'https://bqlf8qjztdtr.statuspage.io/api/v2/summary.json'
    
    config: { heroesUrl: any; textfile: any; };
    data: Config;

    constructor(private http: HttpClient) { }

    getConfig() {
        return this.http.get(this.configUrl);
      }

    ngOnInit(): void {
        console.log("Hello.");
        console.log(this.getConfig());
        this.showConfig();
    }

    showConfig() {
          this.getConfig()
            .subscribe((data: Config) => {
                this.data = data;
                console.log(this.data);}
            );

}

}
