import { Component, OnInit } from '@angular/core';
// import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'protractor';

@Component({
    selector:'app-status',
    templateUrl: './status.component.html',
    styleUrls: ['./status.component.css']
})

// @Injectible()
export class StatusComponent implements OnInit{

    // 'configUrl = https://bqlf8qjztdtr.statuspage.io/api/v2/summary.json'
    configUrl: string = 'https://bqlf8qjztdtr.statuspage.io/api/v2/summary.json'
    
    config: { heroesUrl: any; textfile: any; };
    data: Config;

    constructor(private http: HttpClient) { }

    getConfig() {
        return this.http.get(this.configUrl);
      }

    ngOnInit(): void {
        console.log("Hello.");
        console.log(this.getConfig());
        this.showConfig();
    }

    showConfig() {
          this.getConfig()
            .subscribe((data: Config) => {
                this.data = data;
                console.log(this.data);}
            );

}
}