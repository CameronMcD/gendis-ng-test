import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { StatusComponent } from './components/status/status.component';
import { FirstWidgetComponent } from './components/first-widget/first-widget.component';
// import { OverallWidgetComponent } from './overall-widget/overall-widget.component';

@NgModule({
  declarations: [
    AppComponent, 
    // OverallWidgetComponent,
    StatusComponent, FirstWidgetComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
